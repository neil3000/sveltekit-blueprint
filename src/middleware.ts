import { defineMiddleware } from 'astro:middleware';
import DeviceDetector from 'device-detector-js';

// `context` and `next` are automatically typed
export const onRequest = defineMiddleware((context, next) => {
	// intercept data from a request
	// optionally, modify the properties in `locals`
	const deviceDetector = new DeviceDetector();
	const userAgent = context.request.headers.get('user-agent') || '';
	console.log(context.request.headers);
	const device = deviceDetector.parse(userAgent);
	context.locals.isDesktopDevice = device.device?.type === 'desktop';

	// return a Response or the result of calling `next()`
	return next();
});
