/// <reference types="astro/client" />
/// <reference path="../.astro/types.d.ts" />
/// <reference types="vite-plugin-pwa/info" />
/// <reference types="vite-plugin-pwa/pwa-assets" />
/// <reference types="vite-plugin-pwa/vanillajs" />

declare namespace App {
	interface Locals {
		paraglide: {
			lang: string;
			dir: 'ltr' | 'rtl';
		};
		isDesktopDevice: boolean;
	}
}
