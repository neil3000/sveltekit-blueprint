class AnimationManager {
	private registeredAnimations: ((elapsed: number) => void)[];
	private animationFrameId: number;
	private lastAnimationFrameTime: number;

	constructor() {
		this.registeredAnimations = [];
		this.animationFrameId = -1;
		this.lastAnimationFrameTime = 0;
	}

	register(animationCallback: (elapsed: number) => void): void {
		if (!this.registeredAnimations.includes(animationCallback)) {
			this.registeredAnimations.push(animationCallback);
			this.start();
		}
	}

	unregister(animationCallback: (elapsed: number) => void): void {
		this.registeredAnimations = this.registeredAnimations.filter(cb => cb !== animationCallback);
		if (this.registeredAnimations.length === 0) {
			this.stop();
		}
	}

	start(): void {
		try {
			if (typeof window !== 'undefined' && this.animationFrameId === -1) {
				this.lastAnimationFrameTime = 0;
				this.animationFrameId = window.requestAnimationFrame(this.animate.bind(this));
			}
		} catch (error) {
			console.error('Failed to start animation:', error);
		}
	}

	stop(): void {
		try {
			if (typeof window !== 'undefined' && this.animationFrameId !== -1) {
				window.cancelAnimationFrame(this.animationFrameId);
				this.animationFrameId = -1;
			}
		} catch (error) {
			console.error('Failed to stop animation:', error);
		}
	}

	private animate(timestamp: number): void {
		try {
			if (!this.lastAnimationFrameTime) this.lastAnimationFrameTime = timestamp;
			const elapsed = timestamp - this.lastAnimationFrameTime;

			this.registeredAnimations.forEach(callback => callback(elapsed));
			this.lastAnimationFrameTime = timestamp;

			if (this.registeredAnimations.length > 0) {
				this.animationFrameId = window.requestAnimationFrame(this.animate.bind(this));
			} else {
				this.stop();
			}
		} catch (error) {
			console.error('Animation error:', error);
			this.stop();
		}
	}
}

export const animationManager = new AnimationManager();
