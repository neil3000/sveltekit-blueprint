import DOMPurify from 'dompurify';

interface TrustedTypesPolicy {
	createHTML: (html: string) => TrustedHTML | string;
	createScriptURL: (url: string) => TrustedScriptURL | string;
	createScript: (script: string) => TrustedScript | string;
}

let trustedTypesPolicy: TrustedTypesPolicy;

if (typeof window !== 'undefined' && window.trustedTypes) {
	try {
		trustedTypesPolicy = window.trustedTypes.createPolicy('default', {
			createHTML: (html: string) => DOMPurify.sanitize(html),
			createScriptURL: (url: string) => {
				const parsedUrl = new URL(url, window.location.href);
				if (parsedUrl.origin === window.location.origin) {
					return parsedUrl.href;
				}
				throw new TypeError('Invalid script URL');
			},
			createScript: (script: string) => script,
		});
	} catch (e) {
		console.error('Trusted Types Policy creation failed:', e);
		trustedTypesPolicy = {
			createHTML: (html: string) => DOMPurify.sanitize(html),
			createScriptURL: (url: string) => url,
			createScript: (script: string) => script,
		};
	}
} else {
	trustedTypesPolicy = {
		createHTML: (html: string) => DOMPurify.sanitize(html),
		createScriptURL: (url: string) => url,
		createScript: (script: string) => script,
	};
}

export default trustedTypesPolicy;
