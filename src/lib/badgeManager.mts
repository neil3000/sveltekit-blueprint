class BadgeManager {
	private currentCount: number;

	constructor() {
		this.currentCount = 0;
	}

	isBadgingAPIAvailable(): boolean {
		if (
			typeof navigator !== 'undefined' &&
			'setAppBadge' in navigator &&
			'clearAppBadge' in navigator
		) {
			return true;
		} else {
			console.warn('The Badging API is not available on this platform.');
			return false;
		}
	}

	showDotBadge(): void {
		if (this.isBadgingAPIAvailable()) {
			navigator.setAppBadge().catch((error: Error) => {
				console.error('Error displaying the dot badge:', error);
			});
		}
	}

	clearBadge(): void {
		if (this.isBadgingAPIAvailable()) {
			this.currentCount = 0;
			navigator.clearAppBadge().catch((error: Error) => {
				console.error('Error clearing the badge:', error);
			});
		}
	}

	setBadge(count: number): void {
		if (this.isBadgingAPIAvailable()) {
			this.currentCount = count;
			if (this.currentCount > 0) {
				navigator.setAppBadge(this.currentCount).catch((error: Error) => {
					console.error('Error setting the badge:', error);
				});
			} else {
				this.clearBadge();
			}
		}
	}

	incrementBadge(increment = 1): void {
		this.setBadge(this.currentCount + increment);
	}

	decrementBadge(decrement = 1): void {
		this.setBadge(this.currentCount - decrement);
	}
}

export const badgeManager = new BadgeManager();
