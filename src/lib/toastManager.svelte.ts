import { animationManager } from '$lib/animationManager.mjs';
import { badgeManager } from '$lib/badgeManager.mjs';
// import trustedTypesPolicy from '$lib/trusted-types-policy.mjs';

interface ToastDimensions {
	width: number;
	height: number;
}

interface ToastPosition {
	x: number;
	y: number;
}

interface ToastButton {
	text: string;
	handler: (event: MouseEvent) => void;
}

export interface ToastType {
	id: string;
	index: number;
	message: string;
	count: number;
	progress: number;
	running: boolean;
	priority: number;
	duration: number;
	dimensions: ToastDimensions;
	position: ToastPosition;
	color: string;
	buttons: ToastButton[];
	pauseOnHover: boolean;
	timestamp: number;
	updateToastsPositions: () => void;
}

type StackDirection = 'up' | 'down' | 'left' | 'right';

class ToastStore {
	maxToasts: number;
	preventDuplicateMessages: boolean;
	horizontalAlignPercentage: number;
	verticalAlignPercentage: number;
	stackDirection: StackDirection;
	spacing: number;
	isSpacingRelative: boolean;
	defaultToastDurationMs: number;
	defaultToastBackgroundColor: string;

	toasts: ToastType[];
	toastQueue: ToastType[];
	constructor(
		maxToasts = 3,
		preventDuplicateMessages = true,
		horizontalAlignPercentage = 0,
		verticalAlignPercentage = 100,
		stackDirection: StackDirection = 'up',
		spacing = 10,
		isSpacingRelative = true,
		defaultToastDurationMs = 3000,
		defaultToastBackgroundColor = '#FFFFFF',
	) {
		this.maxToasts = maxToasts;
		this.preventDuplicateMessages = preventDuplicateMessages;
		this.horizontalAlignPercentage = horizontalAlignPercentage;
		this.verticalAlignPercentage = verticalAlignPercentage;
		this.stackDirection = stackDirection;
		this.spacing = spacing;
		this.isSpacingRelative = isSpacingRelative;
		this.defaultToastDurationMs = defaultToastDurationMs;
		this.defaultToastBackgroundColor = defaultToastBackgroundColor;

		this.toastAnimation = this.toastAnimation.bind(this);

		const toastsState = $state([]);
		const toastQueueState = $state([]);

		this.toasts = toastsState;
		this.toastQueue = toastQueueState;
	}

	addToast(options: Partial<ToastType>): ToastType {
		try {
			if (this.preventDuplicateMessages) {
				const toast = this.preventDuplicates(options);
				if (toast) {
					this.updateToast(toast, options);
					return toast;
				}
			}

			const toast = this.createNewToast(options);
			this.processToastInsertion(toast);

			badgeManager.incrementBadge();
			return toast;
		} catch (error) {
			console.error('Failed to add a new toast:', error);
			throw new Error('Error adding new toast.');
		}
	}

	createNewToast(options: Partial<ToastType>): ToastType {
		const id = crypto.randomUUID();
		const toast: ToastType = $state<ToastType>({
			id,
			index: 0,
			message: '',
			count: 1,
			progress: 100,
			running: true,
			priority: 0,
			duration: options.duration ?? this.defaultToastDurationMs,
			dimensions: { width: 0, height: 0 },
			position: { x: 0, y: 0 },
			color: this.defaultToastBackgroundColor,
			buttons: [],
			pauseOnHover: true,
			timestamp: Date.now(),
			...options,
			updateToastsPositions: () => this.updateToastsPositions(toast.index + 1),
		});

		return toast;
	}

	processToastInsertion(toast: ToastType): void {
		if (this.toasts.length >= this.maxToasts && this.toasts[0]) {
			if (toast.priority < this.toasts[0].priority) {
				this.insertToast(toast, this.toastQueue, true);
			} else {
				this.insertToast(this.toasts[0], this.toastQueue, true);
				this.removeToast(this.toasts[0], true);
				this.insertToast(toast, this.toasts);
			}
		} else {
			this.insertToast(toast, this.toasts);
		}
	}

	insertToast(toast: ToastType, toasts: ToastType[], toQueue = false, fromQueue = false): void {
		if (this.toasts.length === 0) {
			animationManager.register(this.toastAnimation);
		}

		if (!fromQueue) {
			this.insertBasedOnPriority(toast, toasts, toQueue);
		} else {
			toast.position = this.calculateToastPosition(0);
			toasts.splice(0, 0, toast);
		}

		toast.index = toasts.findIndex(t => t.id === toast.id);
	}

	insertBasedOnPriority(toast: ToastType, toasts: ToastType[], toQueue: boolean): void {
		const insertIndex = this.findInsertIndex(this.toasts, toast);

		if (!toQueue) {
			toast.position = this.calculateToastPosition(
				insertIndex === -1 ? toasts.length : insertIndex,
			);
		}

		if (insertIndex === -1) {
			toasts.push(toast);
		} else {
			toasts.splice(insertIndex, 0, toast);
		}
	}

	findInsertIndex(toasts: ToastType[], newToast: ToastType): number {
		return toasts.findIndex(
			t =>
				t.priority > newToast.priority ||
				(t.priority === newToast.priority && t.timestamp > newToast.timestamp),
		);
	}

	removeToast(toast: ToastType, toQueue = false): void {
		try {
			const index = this.toasts.indexOf(toast);
			this.toasts.splice(index, 1);
			this.handleQueueAfterRemoval(toQueue);
			if (this.toasts.length > 0) {
				this.updateToastsPositions(index);
			} else {
				animationManager.unregister(this.toastAnimation);
			}
			badgeManager.decrementBadge();
		} catch (error) {
			console.error('Failed to remove toast:', error);
			throw new Error('Error removing toast.');
		}
	}

	clearAllToasts() {
		if (this.toasts.length > 0) {
			animationManager.unregister(this.toastAnimation);
		}

		this.toasts.splice(0, this.toasts.length);
		this.toastQueue.splice(0, this.toastQueue.length);

		badgeManager.clearBadge();
	}

	private handleQueueAfterRemoval(toQueue: boolean): void {
		if (!toQueue && this.toastQueue.length > 0) {
			const nextToast = this.toastQueue.pop();
			if (nextToast) {
				this.insertToast(nextToast, this.toasts, false, true);
			}
		}
	}

	updateToast(toast: ToastType, options: Partial<ToastType>, duplicate = false): void {
		try {
			if (
				this.toasts.some(searchToast => searchToast.id === toast.id) ||
				this.toastQueue.some(searchToast => searchToast.id === toast.id)
			) {
				this.handleDuplicateToastIfNeeded(toast, options);
				this.applyToastUpdates(toast, options, duplicate);
			}
		} catch (error) {
			console.error('Failed to update toast:', error);
			throw new Error('Error updating toast.');
		}
	}

	private handleDuplicateToastIfNeeded(toast: ToastType, options: Partial<ToastType>): void {
		if (this.preventDuplicateMessages) {
			const duplicate = this.preventDuplicates(options);
			if (duplicate && duplicate !== toast) {
				this.removeToast(toast);
				this.updateToast(duplicate, options, true);
			}
		}
	}

	private applyToastUpdates(
		toast: ToastType,
		options: Partial<ToastType>,
		duplicate: boolean,
	): void {
		if (duplicate) {
			toast.count += 1;
		}
		Object.assign(toast, options, { progress: 100, count: toast.count });
	}

	private preventDuplicates(options: Partial<ToastType>): ToastType | undefined {
		return this.toasts.concat(this.toastQueue).find(toast => toast.message === options.message);
	}

	calculateToastPosition(index: number): ToastPosition {
		if (typeof window === 'undefined') {
			return { x: 0, y: 0 };
		}
		const basePosition = {
			x: window.innerWidth * (this.horizontalAlignPercentage / 100),
			y: window.innerHeight * (this.verticalAlignPercentage / 100),
		};
		if (index > 0) {
			const prevToast = this.toasts[index - 1];
			if (prevToast) {
				const additionalSpacing = {
					y: this.isSpacingRelative ? prevToast.dimensions.height + this.spacing : this.spacing,
					x: this.isSpacingRelative ? prevToast.dimensions.width + this.spacing : this.spacing,
				};
				this.adjustPositionByStackDirection(basePosition, additionalSpacing, prevToast);
			}
		}
		return basePosition;
	}

	private adjustPositionByStackDirection(
		basePosition: ToastPosition,
		additionalSpacing: { x: number; y: number },
		prevToast: ToastType,
	): ToastPosition {
		switch (this.stackDirection) {
			case 'up':
				basePosition.y = prevToast.position.y - additionalSpacing.y;
				break;
			case 'down':
				basePosition.y = prevToast.position.y + additionalSpacing.y;
				break;
			case 'left':
				basePosition.x = prevToast.position.x - additionalSpacing.x;
				break;
			case 'right':
				basePosition.x = prevToast.position.x + additionalSpacing.x;
				break;
		}
		return basePosition;
	}

	updateToastsPositions(index: number): void {
		for (let i = index; i < this.toasts.length; i++) {
			const toast = this.toasts[i];
			if (toast) {
				const { x, y } = this.calculateToastPosition(i);
				if (toast.position.x !== x || toast.position.y !== y) {
					toast.position = { x, y };
				}
			}
		}
	}

	toastAnimation = (elapsed: number): void => {
		this.toasts.forEach(toast => {
			if (toast.progress > 0 && toast.running) {
				toast.progress -= (100 * elapsed) / toast.duration;
				if (toast.progress <= 0) {
					this.removeToast(toast);
				}
			}
		});
	};
}

export default ToastStore;
