import { registerSW } from 'virtual:pwa-register';
import { mainToastStore } from '$lib/serviceProviders.mts';
import type { ToastType } from '$lib/toastManager.svelte';

window.addEventListener('load', () => {
	let toast: ToastType;

	const refreshSW = registerSW({
		immediate: true,
		onNeedRefresh() {
			toast = mainToastStore.addToast({
				message: 'New content available, click on reload button to update.',
				color: '#48C774',
				duration: 5000,
				priority: 100,
				buttons: [
					{ text: 'Reload', handler: () => refreshCallback() },
					{ text: 'Close', handler: () => mainToastStore.removeToast(toast) },
				],
				pauseOnHover: true,
			});
		},
		onOfflineReady() {
			mainToastStore.updateToast(toast, {
				message: 'App ready to work offline',
				color: '#FC8EAC',
				duration: 10000,
				priority: 130,
				buttons: [{ text: '<3', handler: () => alert('Coucou') }],
				pauseOnHover: true,
			});
		},
		onRegisteredSW(swScriptUrl) {
			console.log('SW registered: ', swScriptUrl);
		},
	});

	const refreshCallback = () => {
		refreshSW(true)
			.then(() => {
				console.log('Refresh complete.');
			})
			.catch(error => {
				console.error('Failed to refresh:', error);
			});
	};
});
