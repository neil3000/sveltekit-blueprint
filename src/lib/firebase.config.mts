import { initializeApp } from 'firebase/app';
import type { FirebaseApp } from 'firebase/app';
import { getFirestore, Firestore } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';
import type { Auth } from 'firebase/auth';
import { getStorage } from 'firebase/storage';
import type { FirebaseStorage } from 'firebase/storage';
import { getMessaging, getToken, onMessage, deleteToken, isSupported } from 'firebase/messaging';
import type { Messaging, MessagePayload } from 'firebase/messaging';
import { mainToastStore } from '$lib/serviceProviders.mts';

const firebaseConfig = {
	apiKey: 'AIzaSyBgNqZAp6X0XHlsXPKZR6zAoQwBlNMPdL4',
	authDomain: 'sveltekit-blueprint.firebaseapp.com',
	projectId: 'sveltekit-blueprint',
	storageBucket: 'sveltekit-blueprint.appspot.com',
	messagingSenderId: '961132066282',
	appId: '1:961132066282:web:a97c843df91ab335686e93',
	measurementId: 'G-R9HYER0KB7',
};

const app: FirebaseApp = initializeApp(firebaseConfig);

const db: Firestore = getFirestore(app);
const auth: Auth = getAuth(app);
const storage: FirebaseStorage = getStorage(app);

let messaging: Messaging | null = null;

async function initMessaging(): Promise<void> {
	try {
		if (await isSupported()) {
			messaging = getMessaging(app);
		} else {
			console.log('Firebase not supported in this browser');
		}
	} catch (err) {
		console.log('Error initializing messaging:', err);
	}
}

// Handle incoming messages. Called when:
// - a message is received while the app has focus
// - the user clicks on an app notification created by a service worker
//   `messaging.onBackgroundMessage` handler.
async function setupMessaging() {
	await initMessaging();
	if (messaging) {
		onMessage(messaging, payload => {
			console.log('Message received. ', payload);
			// Update the UI to include the received message
			appendMessage(payload);
		});
	} else {
		console.log('Messaging is not available.');
	}
}

setupMessaging();

function resetUI() {
	if (!messaging) {
		console.log('Messaging has not been initialized.');
		return;
	}
	getToken(messaging, {
		vapidKey:
			'BMLaC6LeAeEC_4PeEFzFHGWEnTM98gV_YWOiR4K_7WqY4jRoY7-OQYg1UrSfLWkEjUKmQfHc3-FRafQ-x1BtnY4',
	})
		.then(currentToken => {
			if (currentToken) {
				sendTokenToServer(currentToken);
				showToken(currentToken);
			} else {
				console.log('No registration token available. Request permission to generate one.');
				setTokenSentToServer(false);
			}
		})
		.catch(err => {
			console.log('An error occurred while retrieving token. ', err);
			showToken('Error retrieving registration token.');
			setTokenSentToServer(false);
		});
}

function showToken(currentToken: string) {
	// Show token in console and UI.
	mainToastStore.addToast({
		message: currentToken,
		color: '#48C774',
		duration: 5000,
		priority: 100,
		pauseOnHover: true,
	});
	console.log('Token: ', currentToken);
}

// Send the registration token your application server, so that it can:
// - send messages back to this app
// - subscribe/unsubscribe the token from topics
function sendTokenToServer(currentToken: string) {
	if (!isTokenSentToServer()) {
		console.log('Sending token to server...', currentToken);
		// TODO(developer): Send the current token to your server.
		setTokenSentToServer(true);
	} else {
		console.log("Token already sent to server so won't send it again unless it changes");
	}
}

function isTokenSentToServer() {
	return window.localStorage.getItem('sentToServer') === '1';
}

function setTokenSentToServer(sent: boolean) {
	window.localStorage.setItem('sentToServer', sent ? '1' : '0');
}

export function requestPermission() {
	console.log('Requesting permission...');
	Notification.requestPermission().then(permission => {
		if (permission === 'granted') {
			console.log('Notification permission granted.');
			// TODO(developer): Retrieve a registration token for use with FCM.
			// In many cases once an app has been granted notification permission,
			// it should update its UI reflecting this.
			resetUI();
		} else {
			console.log('Unable to get permission to notify.');
		}
	});
}

function deleteTokenFromFirebase() {
	// Vérifier si `messaging` est initialisé
	if (!messaging) {
		console.log('Messaging service is not initialized.');
		return;
	}
	getToken(messaging)
		.then(currentToken => {
			if (!messaging) {
				console.log('Messaging service is not initialized.');
				return;
			}
			deleteToken(messaging)
				.then(() => {
					console.log('Token deleted.', currentToken);
					setTokenSentToServer(false);
					// Once token is deleted update UI
					resetUI();
				})
				.catch(err => {
					console.log('Unable to delete token. ', err);
				});
		})
		.catch(err => {
			console.log('Error retrieving registration token. ', err);
			showToken('Error retrieving registration token.');
		});
}

// Add a message to the messages element.
function appendMessage(payload: MessagePayload) {
	const title = payload.notification?.title;
	const body = payload.notification?.body;

	const displayMessage = `${title}\n${body}`;

	mainToastStore.addToast({
		message: displayMessage,
		color: '#48C774',
		duration: 5000,
		priority: 100,
		pauseOnHover: true,
	});
	console.log('Token: ', JSON.stringify(payload, null, 2));
}

resetUI();

export { db, auth, storage, messaging };
