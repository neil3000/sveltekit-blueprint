import ToastStore from '$lib/toastManager.svelte';

const mainToastStore = new ToastStore(3, true, 5, 90, 'up', 10, true, 3000, '#FFFFFF');

export { mainToastStore };
