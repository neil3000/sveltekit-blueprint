class VibrationManager {
	isVibrationAPIAvailable(): boolean {
		if ('vibrate' in navigator) {
			return true;
		} else {
			console.warn('The Vibration API is not available on this platform.');
			return false;
		}
	}

	startVibrate(duration: number | number[]): void {
		if (this.isVibrationAPIAvailable()) {
			navigator.vibrate(duration);
		}
	}

	stopVibrate(): void {
		if (this.isVibrationAPIAvailable()) {
			navigator.vibrate(0);
		}
	}
}

export const vibrationManager = new VibrationManager();
