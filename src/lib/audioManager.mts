interface WindowWithWebkitAudioContext extends Window {
	webkitAudioContext?: typeof AudioContext;
}

class AudioManager {
	private context: AudioContext | null;
	private audioBuffers: Record<string, AudioBuffer>;
	private audioSources: Record<
		string,
		{ element: HTMLMediaElement; source: MediaElementAudioSourceNode }
	>;

	private gainNode: GainNode | null;
	private isPlaying: boolean;

	constructor() {
		this.context = null;
		this.audioBuffers = {};
		this.audioSources = {};
		this.gainNode = null;
		this.isPlaying = false;
	}

	async initAudioContext(): Promise<void> {
		if (!this.context) {
			const AudioContextConstructor =
				'AudioContext' in window
					? AudioContext
					: 'webkitAudioContext' in window
						? (window as WindowWithWebkitAudioContext).webkitAudioContext
						: undefined;

			if (!AudioContextConstructor) {
				throw new Error('Web Audio API is not supported in this browser');
			}

			this.context = new AudioContextConstructor();
			this.gainNode = this.context.createGain();
			this.gainNode.connect(this.context.destination);
		}
		if (this.context.state === 'suspended') {
			await this.context.resume();
		}
	}

	async loadAudioFromUrl(id: string, url: string): Promise<void> {
		const response = await fetch(url);
		const arrayBuffer = await response.arrayBuffer();
		const audioBuffer = await this.context!.decodeAudioData(arrayBuffer);
		this.audioBuffers[id] = audioBuffer;
	}

	loadAudioFromFile(id: string, file: File): void {
		const reader = new FileReader();
		reader.onload = async (e: ProgressEvent<FileReader>) => {
			const arrayBuffer = e.target!.result as ArrayBuffer;
			const audioBuffer = await this.context!.decodeAudioData(arrayBuffer);
			this.audioBuffers[id] = audioBuffer;
		};
		reader.readAsArrayBuffer(file);
	}

	attachAudioElement(id: string, audioElement: HTMLMediaElement): void {
		const source = this.context!.createMediaElementSource(audioElement);
		source.connect(this.gainNode!);
		this.audioSources[id] = { element: audioElement, source };
	}

	async playSound(id: string): Promise<void> {
		const buffer = this.audioBuffers[id];
		const audioSource = this.audioSources[id];

		if (buffer) {
			const source = this.context!.createBufferSource();
			source.buffer = buffer;
			source.connect(this.gainNode!);
			source.start(0);
			source.onended = () => {
				source.disconnect();
				this.isPlaying = false;
			};
			this.isPlaying = true;
		} else if (audioSource?.element) {
			try {
				await audioSource.element.play();
				this.isPlaying = true;
			} catch (error) {
				console.error('Error trying to play audio:', error);
			}
		} else {
			console.error('No audio source found for id:', id);
		}
	}

	get playing(): boolean {
		return this.isPlaying;
	}

	setGain(value: number): void {
		this.gainNode!.gain.value = value;
	}
}

export const audioManager = new AudioManager();
