import type { Config } from 'tailwindcss';

const config: Config = {
	content: ['./src/**/*.{astro,html,js,jsx,mjs,md,mdx,svelte,ts,tsx,mts,vue}'],
	theme: {
		extend: {},
	},
	plugins: [],
};

export default config;
