import { test, expect } from '@playwright/test';
import type { Page } from '@playwright/test';

interface LocalizationTestParams {
	page: Page;
	url: string;
	languageHeader: string;
	selector: string;
	expectedText: string;
}

/**
 * Runs a localization test by checking if the specified text content matches the expected text.
 * @param {LocalizationTestParams} params - The parameters object.
 */
const runLocalizationTest = async ({
	page,
	url,
	languageHeader,
	selector,
	expectedText,
}: LocalizationTestParams): Promise<void> => {
	// await page.setExtraHTTPHeaders({
	//   'Accept-Language': languageHeader
	// });

	// await page.goto(url);

	// expect(page.url()).toContain(expectedUrlPart);

	await page.goto(`${url}/fr`);

	const localizedText = await page.textContent(selector);
	expect(localizedText).toBe(expectedText);
};

test.describe('Localization and Routing Tests', () => {
	test('generic localized messages test', async ({ page }) => {
		await runLocalizationTest({
			page,
			url: 'http://localhost:4173',
			languageHeader: 'fr',
			// expectedUrlPart: '/fr',
			selector: 'h1',
			expectedText: 'Bienvenue sur SvelteKit',
		});
	});
});
