import { test as baseTest } from '@playwright/test';
import type { Page } from '@playwright/test';

export const test = baseTest.extend<{ page: Page }>({
	page: async ({ page }, use) => {
		page.on('console', msg => {
			console.log(`BROWSER LOG: ${msg.text()}`);
		});
		await use(page);
	},
});
