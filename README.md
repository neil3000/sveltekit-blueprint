<div align='center'>
    <h1><b>svelte-blueprint</b></h1>
    <a><img src='./assets/sveltekit-blueprint_logo.png' width='200'/></a>
    <p>A Svelte template with lots of features</p>

<a>![Status](https://badgen.net/badge/Status/Alpha/red?)</a>

</div>

---

# Features

- [ ] 404 error handling
- [ ] Google Analytics (via Firebase)
- [ ] Support Remote Config
- [ ] PWA
- [ ] GDPR
- [ ] Authentication
- [ ] WCAG
- [ ] i18n
- [ ] Payments
- [ ] Crashlytics / Performance analytics
- [ ] Logs management
- [ ] SendFeedback Modal (w/ logs)
- [ ] Theme handling (Dark/light theme)
- [ ] ViewTransitions

# Docs

[![Achievements](https://media.discordapp.net/attachments/458971493280514049/1150810894188679238/f14jxly.png)](./docs/achievements/doc.md)&nbsp;
[![Giveaways](https://media.discordapp.net/attachments/458971493280514049/1150810661388046367/Bv2iIrM.png)](./docs/giveaways/doc.md)&nbsp;

# This repository

[![Issue board](https://media.discordapp.net/attachments/346737406591893506/1146859978813087925/co.rahmouni.dev__7.png)](https://gitlab.com/neil3000/sveltekit-blueprint/-/boards)&nbsp;

<br/>

Issues / tasks are all categorized using the following labels:

- **Priorities:** ~P0 ~P1 ~P2 ~P3 ~P4
- **Progress:** ~Planned ~Doing
- **Type (Issue):** ~Feature ~enhancement
- **Type (Meta):** ~documentation ~refactor
- **Bugs:** ~bug (& ~confirmed if replicable)

Progress labels are used as categories on the [issue board](https://url.creative-olympics.org/board).

Priorities follow the [Google Issue Tracker issue priorities](https://developers.google.com/issue-tracker/concepts/issues#priority) guidelines:

| Priority | Description                                                                                                                                                                                                                                                                                                                                                                                                 |
| -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ~P0      | An issue that needs to be addressed immediately and with as many resources as is required. Such an issue causes a full outage or makes a critical function of the product to be unavailable for everyone, without any known workaround.                                                                                                                                                                     |
| ~P1      | An issue that needs to be addressed quickly. Such an issue significantly impacts a large percentage of users; if there is a workaround it is partial or overly painful. The impact of the issue is to a core organizational function, or fundamentally impedes another team.                                                                                                                                |
| ~P2      | An issue that needs to be addressed on a reasonable timescale. Such an issue could be any of the following: <br/>・ An issue that would be ~P0 or ~P1 but has a reasonable workaround<br/>・ An issue that is important to a large percentage of users and is connected to core organizational functions<br/>・ An issue that is an impediment to the work of other teams and has no reasonable workaround. |
| ~P3      | An issue that should be addressed when able. Such an issue is relevant to core organizational functions or the work of other teams, but does not impede progress or else has a reasonable workaround.                                                                                                                                                                                                       |
| ~P4      | An issue that should be addressed eventually. Such an issue is not relevant to core organizational functions or the work of other teams, or else it relates only to the attractiveness or pleasantness of the system.                                                                                                                                                                                       |

**Scripts:**

Run the website in dev mode:

```bash
npm run dev
# concurrently \"vite build\" \"cross-env TAILWIND_MODE=watch cross-env NODE_ENV=development postcss src/app.css -o src/app-output.css -w\"
```

Build a production version of the website:

```bash
npm run build
# npm run cross-env TAILWIND_MODE=build cross-env NODE_ENV=production postcss src/app.css -o src/app-output.css && vite build
```

Preview the production version of the website:

```bash
npm run preview
# vite preview
```

<br/>

<details>
<summary><a><img src="https://media.discordapp.net/attachments/346737406591893506/1146877882354897036/co.rahmouni.dev__8.png"/></a></summary>
<br/>

ONLY Run the website in dev mode (without Tailwind):

```bash
npm run dev:only
# vite build
```

ONLY Build a production version of the website (without Tailwind):

```bash
npm run build:only
# vite build
```

Tailwind watch:

```bash
npm run tailwind:watch
# cross-env TAILWIND_MODE=watch cross-env NODE_ENV=development postcss src/app.css -o src/app-output.css -w
```

ONLY Build a production version of Tailwind (without the website):

```bash
npm run tailwind:build
# cross-env TAILWIND_MODE=build cross-env NODE_ENV=production postcss src/app.css -o src/app-output.css
```

Run the website in dev mode and make it available to the local network (or the whole internet if you open the ports in your router):

```bash
npm run devhost
# concurrently \"npm run dev:only -- --host\" \"npm run tailwind:watch\"
```

Lint:

```bash
npm run lint
# prettier --plugin-search-dir . --check . && eslint .
```

Format:

```bash
npm run format
# prettier --plugin-search-dir . --write .
```

</details>

# create-svelte

Everything you need to build a Svelte project, powered by [`create-svelte`](https://github.com/sveltejs/kit/tree/main/packages/create-svelte).

## Creating a project

If you're seeing this, you've probably already done this step. Congrats!

```bash
# create a new project in the current directory
npm create svelte@latest

# create a new project in my-app
npm create svelte@latest my-app
```

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
