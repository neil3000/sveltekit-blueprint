import { promises as fs } from 'fs';
import { minify } from 'html-minifier';
import { globby } from 'globby';
import JavaScriptObfuscator from 'javascript-obfuscator';
import type { AstroIntegration } from 'astro';
import path from 'path';

const titleColor = '\x1b[38;2;181;255;89m'; // #b5ff59
const textColor = '\x1b[38;2;163;89;255m'; // #a359ff
const redColor = '\x1b[38;2;234;66;88m'; // Red color
const resetColor = '\x1b[0m';

function logTitle(title: String) {
	console.log(`${titleColor}${title}${resetColor}`);
}

function logMessage(message: String) {
	console.log(`• ${textColor}${message}${resetColor}`);
}

function logRedMessage(message: String) {
	console.log(`• ${redColor}${message}${resetColor}`);
}

export default function postbuildIntegration(): AstroIntegration {
	return {
		name: 'postbuild-integration',
		hooks: {
			'astro:build:done': async () => {
				const minifyOptions = {
					removeComments: true,
					collapseWhitespace: true,
					preserveLineBreaks: true,
				};

				async function processHtmlFiles() {
					const htmlFiles = await globby(`dist/**/*.html`);

					if (htmlFiles.length > 0) {
						logTitle('Processing HTML files:');
						await Promise.all(
							htmlFiles.map(async file => {
								let html = await fs.readFile(file, 'utf-8');
								const beforeSize = Buffer.byteLength(html, 'utf8');

								html = minify(html, minifyOptions);

								await fs.writeFile(file, html);

								const afterSize = Buffer.byteLength(html, 'utf8');

								logMessage(`${file} - Before: ${beforeSize} bytes, After: ${afterSize} bytes`);
							}),
						);
					} else {
						logMessage('No HTML files found.');
					}
				}

				async function obfuscateJsFiles() {
					const jsFiles = await globby(`dist/**/*.js`);

					if (jsFiles.length > 0) {
						logTitle('Obfuscating JS files:');
						await Promise.all(
							jsFiles.map(async file => {
								let code = await fs.readFile(file, 'utf-8');

								const beforeSize = Buffer.byteLength(code, 'utf8');

								const obfuscatedCode = JavaScriptObfuscator.obfuscate(code, {
									compact: true,
									controlFlowFlattening: false,
									deadCodeInjection: false,
									debugProtection: false,
									identifierNamesGenerator: 'mangled-shuffled',
									numbersToExpressions: false,
									renameGlobals: false,
									selfDefending: false,
									simplify: false,
									splitStrings: false,
									stringArray: true,
									transformObjectKeys: false,
									stringArrayCallsTransform: false,
									stringArrayEncoding: [],
									stringArrayIndexShift: false,
									stringArrayRotate: false,
									stringArrayShuffle: false,
									stringArrayWrappersCount: 0,
									stringArrayWrappersChainedCalls: false,
									stringArrayWrappersParametersMaxCount: 2,
									stringArrayWrappersType: 'variable',
									stringArrayThreshold: 1,
									unicodeEscapeSequence: false,
									renamePropertiesMode: 'safe',
									renameProperties: false,
								}).getObfuscatedCode();

								const afterSize = Buffer.byteLength(obfuscatedCode, 'utf8');

								const filename = path.basename(file);
								const shouldNotDelete = filename.includes('astro');

								if (obfuscatedCode.length === 0 && !shouldNotDelete) {
									await fs.unlink(file);
									logRedMessage(`Deleted empty file: ${file}`);
								} else {
									await fs.writeFile(file, obfuscatedCode);
									logMessage(`${file} - Before: ${beforeSize} bytes, After: ${afterSize} bytes`);
								}
							}),
						);
					} else {
						logMessage('No JS files found.');
					}
				}

				try {
					await processHtmlFiles();
					// await obfuscateJsFiles();
					logTitle('Post-build tasks completed successfully.');
				} catch (error) {
					console.error(
						`${redColor}Error during post-build tasks:${resetColor} ${textColor}${error}${resetColor}`,
					);
				}
			},
		},
	};
}
