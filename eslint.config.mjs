import path from 'path';
import { fileURLToPath } from 'url';
import globals from 'globals';
import js from '@eslint/js';

import tseslint from 'typescript-eslint';

// plugins
import eslintConfigPrettier from 'eslint-config-prettier';
import eslintPluginSvelte from 'eslint-plugin-svelte';
import regexpEslint from 'eslint-plugin-regexp';
const typescriptEslint = tseslint.plugin;

// parsers
const typescriptParser = tseslint.parser;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export default [
	js.configs.recommended,
	...tseslint.configs.recommended,
	...tseslint.configs.recommendedTypeChecked,
	regexpEslint.configs['flat/recommended'],
	...eslintPluginSvelte.configs['flat/recommended'],
	eslintConfigPrettier,
	{
		ignores: [
			'.DS_Store',
			'**/node_modules/',
			'**/build/',
			'**/paraglide/',
			'**/dist/',
			'.env',
			'.env.*',
			'!.env.example',
			'pnpm-lock.yaml',
			'**/.*',
			'**/*.d.ts',
			'**/*.component.ts',
		],
	},
	{
		languageOptions: {
			parser: typescriptParser,
			globals: {
				$state: 'writable',
				...globals.browser,
			},
			parserOptions: {
				sourceType: 'module',
				ecmaVersion: 2024,
				extraFileExtensions: ['.svelte'],
				project: ['./packages/*/tsconfig.json', './tsconfig.json'],
				tsconfigRootDir: __dirname,
			},
		},
		plugins: {
			'@typescript-eslint': typescriptEslint,
			regexp: regexpEslint,
			svelte: eslintPluginSvelte,
			prettier: eslintConfigPrettier,
		},
	},
];
