// Astro related imports
import { defineConfig } from 'astro/config';
import node from '@astrojs/node';
import sitemap from '@astrojs/sitemap';
import compressor from 'astro-compressor';

// Astro integrations
import tailwind from '@astrojs/tailwind';
import svelte, { vitePreprocess } from '@astrojs/svelte';
import angular from '@analogjs/astro-angular';

// Testing libraries
import { svelteTesting } from '@testing-library/svelte/vite';

// Progressive Web App (PWA) and other specific utilities
import AstroPWA from '@vite-pwa/astro';
import pwaConfiguration from './pwa.config.mts';

// Paraglide and inLang integrations
import paraglide from '@inlang/paraglide-astro';

import { shield } from '@kindspells/astro-shield';
import postbuildIntegration from './postbuild.mts';

export default defineConfig({
	vite: {
		plugins: [svelteTesting()],
		server: {
			host: '0.0.0.0',
			fs: {
				allow: ['..'],
			},
		},
		build: {
			cssMinify: 'lightningcss',
			assetsInlineLimit: 0,
		},
		ssr: {
			noExternal: ['@rx-angular/**'],
		},
	},
	site: 'https://sveltekit-blueprint.com',
	i18n: {
		defaultLocale: 'en',
		locales: ['en', 'fr'],
	},
	integrations: [
		compressor(),
		AstroPWA(pwaConfiguration),
		tailwind(),
		shield({
			securityHeaders: {
				contentSecurityPolicy: {},
			},
		}),
		sitemap({
			i18n: {
				defaultLocale: 'en',
				locales: {
					en: 'en',
					fr: 'fr',
				},
			},
		}),
		svelte({
			preprocess: [vitePreprocess()],
		}),
		angular({
			vite: {
				inlineStylesExtension: 'scss|sass|less',
			},
		}),
		paraglide({
			project: './project.inlang',
			outdir: './src/paraglide',
		}),
		postbuildIntegration(),
	],
	output: 'static',
	prefetch: {
		prefetchAll: true,
		defaultStrategy: 'viewport',
	},
	experimental: {
		clientPrerender: true,
	},
	adapter: node({
		mode: 'standalone',
	}),
});
