/// <reference types="vitest" />
import { getViteConfig } from 'astro/config';

export default getViteConfig({
	test: {
		reporters: ['verbose', 'junit'],
		outputFile: {
			junit: './junit-report.xml',
		},
		coverage: {
			all: true,
			provider: 'v8',
			reporter: 'text',
			exclude: [
				'coverage/**',
				'**/[.]**',
				'packages/*/test?(s)/**',
				'**/*.d.ts',
				'test?(s)/**',
				'*.config.js',
				'**/paraglide/**',
			],
		},
		include: [
			'tests/unit/**/*.{test,spec}.{js,mjs,ts,mts}',
			'packages/**/tests/unit/**/*.{test,spec}.{js,mjs,ts,mts}',
		],
		environment: 'jsdom',
	},
});
