import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { IconButtonComponent } from '@svelte-blueprint/angular-components/default/buttons/iconButton.component.ts';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';

@Component({
	selector: 'mobile-toolbar',
	template: `
		<mat-toolbar class="mobile-toolbar">
			<mat-toolbar-row class="top-toolbar">
				<h1>{{ title }}</h1>
				<ng-container *ngFor="let icon of iconList">
					<app-icon-button class="toolbar-icon" [fontIcon]="icon" />
				</ng-container>
			</mat-toolbar-row>
			<mat-toolbar-row class="bottom-toolbar">
				<ng-container *ngFor="let nav of navList">
					<a mat-button>
						<mat-icon [fontSet]="nav.active ? 'material-icons' : 'material-icons-outlined'">
							{{ nav.icon }}
						</mat-icon>
						{{ nav.label }}
					</a>
				</ng-container>
			</mat-toolbar-row>
		</mat-toolbar>
	`,
	styleUrls: ['./mobileToolbar.scss'],
	standalone: true,
	imports: [IconButtonComponent, MatButtonModule, CommonModule, MatToolbarModule, MatIconModule],
})
export class MobileToolbar {
	@Input() title: string;
	@Input() iconList: string[];
	@Input() navList: NavItem[];
	@Input() isDesktopDevice: boolean = false;
}

interface NavItem {
	label: string;
	icon: string;
	active: boolean;
}
