import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlatAnchorComponent } from '@svelte-blueprint/angular-components/default/anchors/flatAnchor.component.ts';
import { IconButtonComponent } from '@svelte-blueprint/angular-components/default/buttons/iconButton.component.ts';
import { MatToolbarModule } from '@angular/material/toolbar';

@Component({
	selector: 'desktop-toolbar',
	template: `
		<mat-toolbar class="desktop-toolbar">
			<h1>{{ title }}</h1>
			<div>
				<ng-container *ngFor="let nav of navList">
					<app-flat-anchor [text]="nav"></app-flat-anchor>
				</ng-container>
			</div>
			<div>
				<ng-container *ngFor="let icon of iconList">
					<app-icon-button [fontIcon]="icon"></app-icon-button>
				</ng-container>
			</div>
		</mat-toolbar>
	`,
	styleUrls: ['./desktopToolbar.scss'],
	standalone: true,
	imports: [FlatAnchorComponent, IconButtonComponent, CommonModule, MatToolbarModule],
})
export class DesktopToolbar {
	@Input() title: string;
	@Input() navList: string[];
	@Input() iconList: string[];
}
