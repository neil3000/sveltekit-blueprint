import { Component, Input } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';

@Component({
	selector: 'app-icon',
	template: `<mat-icon [fontSet]="fontSet" [inline]="inline" [svgIcon]="svgIcon">{{
		fontIcon
	}}</mat-icon>`,
	standalone: true,
	imports: [MatIconModule],
})
export class IconComponent {
	@Input() fontIcon: string;
	@Input() fontSet: string = 'material-icons-outlined';
	@Input() inline: boolean = false;
	@Input() svgIcon: string;
}
