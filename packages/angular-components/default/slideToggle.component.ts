import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@Component({
	selector: 'app-slide-toggle',
	template: `
		<mat-slide-toggle
			[aria-describedby]="ariaDescribedby"
			[aria-label]="ariaLabel"
			[aria-labelledby]="ariaLabelledby"
			[checked]="checked"
			[disableRipple]="disableRipple"
			[disabled]="disabled"
			[disabledInteractive]="disabledInteractive"
			[hideIcon]="hideIcon"
			[id]="id"
			[labelPosition]="labelPosition"
			[name]="name"
			[required]="required"
			(change)="change.emit($event)"
			(toggleChange)="toggleChange.emit()"
		>
			{{ label }}
		</mat-slide-toggle>
	`,
	standalone: true,
	imports: [MatSlideToggleModule],
})
export class SlideToggleComponent {
	@Input('aria-describedby') ariaDescribedby: string;
	@Input('aria-label') ariaLabel: string | null;
	@Input('aria-labelledby') ariaLabelledby: string | null;
	@Input() checked: boolean = false;
	@Input() disableRipple: boolean = false;
	@Input() disabled: boolean = false;
	@Input() disabledInteractive: boolean = false;
	@Input() hideIcon: boolean = false;
	@Input() id: string;
	@Input() labelPosition: 'before' | 'after' = 'after';
	@Input() name: string | null;
	@Input() required: boolean = false;
	@Input() label: string;

	@Output() change: EventEmitter<any> = new EventEmitter<any>();
	@Output() toggleChange: EventEmitter<void> = new EventEmitter<void>();

	constructor() {}
}
