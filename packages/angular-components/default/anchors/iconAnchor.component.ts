import { Component, Input } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';

@Component({
	selector: 'app-icon-anchor',
	template: ` <a
		mat-icon-button
		[attr.aria-disabled]="ariaDisabled ? 'true' : null"
		[disabled]="disabled"
		[class.disabled-interactive]="disabledInteractive"
		[matBadge]="badgeContent"
		[matBadgeDescription]="badgeDescription"
		[matBadgeDisabled]="badgeDisabled"
		[matBadgeHidden]="badgeHidden"
		[matBadgeOverlap]="badgeOverlap"
		[matBadgePosition]="badgePosition"
		[matBadgeSize]="badgeSize"
		[matTooltip]="tooltipMessage"
		[matTooltipDisabled]="tooltipDisabled"
		[matTooltipPosition]="tooltipPosition"
		[matTooltipShowDelay]="tooltipShowDelay"
		[matTooltipHideDelay]="tooltipHideDelay"
		[matTooltipClass]="tooltipClass"
		[matTooltipTouchGestures]="tooltipTouchGestures"
	>
		<mat-icon [fontSet]="fontSet" [inline]="inline" [svgIcon]="svgIcon">{{ fontIcon }}</mat-icon>
	</a>`,
	standalone: true,
	imports: [MatButtonModule, MatBadgeModule, MatIconModule, MatTooltipModule],
})
export class IconAnchorComponent {
	@Input() fontIcon: string;
	@Input() fontSet: string = 'material-icons-outlined';
	@Input() inline: boolean = false;
	@Input() svgIcon: string;

	@Input() ariaDisabled: boolean | undefined;
	@Input() disableRipple: boolean;
	@Input() disabled: boolean;
	@Input() disabledInteractive: boolean;

	@Input() badgeContent: string | number | undefined | null;
	@Input() badgeDescription: string;
	@Input() badgeDisabled: boolean = false;
	@Input() badgeHidden: boolean = false;
	@Input() badgeOverlap: boolean = true;
	@Input() badgePosition: 'above before' | 'above after' | 'below before' | 'below after' =
		'above after';
	@Input() badgeSize: 'small' | 'medium' | 'large' = 'medium';

	@Input() tooltipMessage: string;
	@Input() tooltipDisabled: boolean = false;
	@Input() tooltipPosition: 'above' | 'below' | 'left' | 'right' | 'before' | 'after' = 'below';
	@Input() tooltipShowDelay: number = 0;
	@Input() tooltipHideDelay: number = 0;
	@Input() tooltipClass: any;
	@Input() tooltipTouchGestures: 'auto' | 'on' | 'off' = 'auto';
}
