import { Component, Input } from '@angular/core';
import { MatBadgeModule } from '@angular/material/badge';

@Component({
	selector: 'app-badge-icon',
	template: `
		<span
			matBadge="{{ badgeContent }}"
			[matBadgeDescription]="description"
			[matBadgeDisabled]="disabled"
			[matBadgeHidden]="hidden"
			[matBadgeOverlap]="overlap"
			[matBadgePosition]="position"
			[matBadgeSize]="size"
		>
			<mat-icon [fontSet]="fontSet" [inline]="inline" [svgIcon]="svgIcon">{{ fontIcon }}</mat-icon>
		</span>
	`,
	standalone: true,
	imports: [MatBadgeModule],
})
export class BadgeIconComponent {
	@Input() fontIcon: string;
	@Input() fontSet: string = 'material-icons-outlined';
	@Input() inline: boolean = false;
	@Input() svgIcon: string;

	@Input() badgeContent: string | number | undefined | null;
	@Input() content: string;
	@Input() description: string;
	@Input() disabled: boolean = false;
	@Input() hidden: boolean = false;
	@Input() overlap: boolean = flase;
	@Input() position: 'above before' | 'above after' | 'below before' | 'below after' =
		'above after';
	@Input() size: 'small' | 'medium' | 'large' = 'medium';
}
