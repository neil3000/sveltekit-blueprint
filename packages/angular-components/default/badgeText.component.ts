import { Component, Input } from '@angular/core';
import { MatBadgeModule } from '@angular/material/badge';

@Component({
	selector: 'app-badge-text',
	template: `<span
		matBadge="{{ badgeContent }}"
		[matBadgeDescription]="description"
		[matBadgeDisabled]="disabled"
		[matBadgeHidden]="hidden"
		[matBadgeOverlap]="overlap"
		[matBadgePosition]="position"
		[matBadgeSize]="size"
	>
		{{ content }}
	</span> `,
	standalone: true,
	imports: [MatBadgeModule],
})
export class BadgeTextComponent {
	@Input() badgeContent: string | number | undefined | null;
	@Input() content: string;
	@Input() description: string;
	@Input() disabled: boolean = false;
	@Input() hidden: boolean = false;
	@Input() overlap: boolean = false;
	@Input() position: 'above before' | 'above after' | 'below before' | 'below after' =
		'above after';
	@Input() size: 'small' | 'medium' | 'large' = 'medium';
}
