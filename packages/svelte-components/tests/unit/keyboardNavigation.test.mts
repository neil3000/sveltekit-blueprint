import { describe, it, expect, vi, beforeEach, afterEach } from 'vitest';
import { cleanup, render, fireEvent, fireEvent as tlFireEvent } from '@testing-library/svelte';
import KeyboardNavigation from '../../utils/keyboardNavigation.svelte';

describe('KeyboardNavigation', () => {
	let mainContent: HTMLElement;

	beforeEach(() => {
		mainContent = document.createElement('main');
		mainContent.tabIndex = -1;
		document.body.appendChild(mainContent);
	});

	afterEach(() => {
		cleanup();
		document.body.removeChild(mainContent);
	});

	it('becomes visible and focuses the button when Tab is first pressed', async () => {
		const { queryByText, getByText } = render(KeyboardNavigation);
		expect(queryByText('Skip to main content')).toBeNull();

		await tlFireEvent.keyDown(window, { key: 'Tab' } as KeyboardEventInit);
		const button = getByText('Skip to main content');
		expect(button).toBeDefined();
		expect(document.activeElement).toBe(button);
	});

	it('focuses on main content when skip link button is clicked', async () => {
		const { getByText } = render(KeyboardNavigation);
		await tlFireEvent.keyDown(window, { key: 'Tab' } as KeyboardEventInit);
		const button = getByText('Skip to main content');
		vi.spyOn(mainContent, 'focus').mockImplementation(() => {});

		await fireEvent.click(button);
		expect(mainContent.focus).toHaveBeenCalled();
		expect(mainContent).toBe(document.activeElement);
	});

	it('hides the skip link when focus moves away from the button', async () => {
		const { queryByText, getByText } = render(KeyboardNavigation);
		await tlFireEvent.keyDown(window, { key: 'Tab' } as KeyboardEventInit);

		const button = getByText('Skip to main content');
		await fireEvent.focusOut(button);
		expect(queryByText('Skip to main content')).toBeNull();
	});
});
