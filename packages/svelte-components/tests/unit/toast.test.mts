import { describe, it, expect, vi, beforeEach, afterEach } from 'vitest';
import { cleanup, render, fireEvent } from '@testing-library/svelte';
import Toast from '../../utils/toast.svelte';

interface Button {
	text: string;
	handler: () => void;
}

interface ToastProps {
	id: string;
	index: number;
	message: string;
	count: number;
	progress: number;
	running: boolean;
	priority: number;
	duration: number;
	dimensions: { width: number; height: number };
	position: { x: number; y: number };
	color: string;
	buttons: Button[];
	pauseOnHover: boolean;
	timestamp: number;
	updateToastsPositions: (index: number) => void;
}

interface Props {
	toast: ToastProps;
}

describe('Toast component', () => {
	let props: Props;
	let updateToastsPositionsMock: (index: number) => void;

	beforeEach(() => {
		updateToastsPositionsMock = vi.fn();
		props = {
			toast: {
				id: 'toast1',
				index: 0,
				message: 'Test message',
				count: 1,
				progress: 50,
				running: true,
				priority: 1,
				duration: 3000,
				dimensions: { width: 100, height: 50 },
				position: { x: 100, y: 100 },
				color: 'blue',
				buttons: [
					{
						text: 'Click me',
						handler: vi.fn(),
					},
				],
				pauseOnHover: true,
				timestamp: Date.now(),
				updateToastsPositions: updateToastsPositionsMock,
			},
		};
	});

	afterEach(() => {
		cleanup();
	});

	it('renders correctly with initial props', () => {
		const { getByText, getByRole } = render(Toast, { props });
		const message = getByText('Test message');
		const button = getByRole('button', { name: 'Click me' });

		expect(message).toBeTruthy();
		expect(button).toBeTruthy();
	});

	it('calls updateToastsPositions on mount', async () => {
		render(Toast, { props });
		expect(updateToastsPositionsMock).toHaveBeenCalledWith(0);
	});

	it('pauses progress when mouse enters if pauseOnHover is true', async () => {
		const { getByRole } = render(Toast, { props });
		const alert = getByRole('alert');
		fireEvent.mouseEnter(alert);
		expect(props.toast.running).toBe(false);
	});

	it('resumes progress when mouse leaves if pauseOnHover is true', async () => {
		const { getByRole } = render(Toast, { props });
		const alert = getByRole('alert');
		fireEvent.mouseEnter(alert);
		fireEvent.mouseLeave(alert);
		expect(props.toast.running).toBe(true);
	});

	it('button click calls the handler', async () => {
		const { getByRole } = render(Toast, { props });
		const button = getByRole('button', { name: 'Click me' });
		await fireEvent.click(button);
		expect(props.toast.buttons[0]?.handler).toHaveBeenCalled();
	});
});
