// @vitest-environment jsdom
import { describe, it, expect, vi, beforeEach, afterEach } from 'vitest';
import { cleanup, render, fireEvent, screen } from '@testing-library/svelte/svelte5';
import ExternalLink from '../../utils/externalLink.svelte';

describe('ExternalLink component', () => {
	let originalLocation: Location;

	beforeEach(() => {
		originalLocation = window.location;
		Object.defineProperty(window, 'location', {
			value: {
				...originalLocation,
				origin: 'http://localhost',
				assign: vi.fn(),
				reload: vi.fn(),
			},
			writable: true,
		});
	});

	afterEach(() => {
		Object.defineProperty(window, 'location', {
			value: originalLocation,
			writable: true,
		});
		cleanup();
	});

	it('does not show the popup when clicking a local link', async () => {
		const { getByRole } = render(ExternalLink, {
			props: { href: 'http://localhost/page2' },
		});
		const link = getByRole('link');
		await fireEvent.click(link);
		expect(screen.queryByRole('alertdialog')).toBeNull();
	});

	it('shows the popup when clicking an external link', async () => {
		const { getByRole } = render(ExternalLink, {
			props: { href: 'http://external.com/page' },
		});
		const link = getByRole('link');
		await fireEvent.click(link);
		expect(screen.getByRole('alertdialog')).toBeTruthy();
		expect(screen.getByText('external.com')).toBeTruthy();
	});

	it('closes the popup and does not navigate when clicking "No"', async () => {
		const { getByRole, getByText } = render(ExternalLink, {
			props: { href: 'http://external.com/page' },
		});
		const link = getByRole('link');
		await fireEvent.click(link);
		const noButton = getByText('No');
		await fireEvent.click(noButton);
		expect(screen.queryByRole('alertdialog')).toBeNull();
	});

	it('navigates to the external link when clicking "Yes"', async () => {
		const { getByRole, getByText } = render(ExternalLink, {
			props: { href: 'http://external.com/page', target: '_blank' },
		});
		const link = getByRole('link');
		await fireEvent.click(link);
		const yesButton = getByText('Yes');
		vi.spyOn(window, 'open').mockImplementation(() => null);
		await fireEvent.click(yesButton);
		expect(window.open).toHaveBeenCalledWith('http://external.com/page', '_blank');
	});
});
