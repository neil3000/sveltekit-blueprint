import type { PwaOptions } from '@vite-pwa/astro';

const pwaConfiguration: PwaOptions = {
	strategies: 'injectManifest',
	filename: 'sw.ts',
	registerType: 'prompt',
	injectRegister: 'script-defer',
	srcDir: './public',
	mode: 'development',
	scope: '/',
	base: '/',
	manifest: {
		short_name: 'svelte-blueprint',
		name: 'svelte-blueprint',
		id: 'svelte-blueprint',
		start_url: '/',
		scope: '/',
		categories: ['developer tools', 'developer', 'development'],
		description: 'A Svelte template with lots of features',
		display: 'standalone',
		theme_color: '#2683db',
		background_color: '#2683db',
		orientation: 'portrait-primary',
		// iarc_rating_id: "",
		// screenshots: [],
		// shortcuts: [],
		// related_applications: [],
		// prefer_related_applications: true
		icons: [
			{
				src: '/media/android/android-chrome-36x36.png',
				sizes: '36x36',
				type: 'image/png',
				purpose: 'any maskable',
			},
			{
				src: '/media/android/android-chrome-48x48.png',
				sizes: '48x48',
				type: 'image/png',
				purpose: 'any maskable',
			},
			{
				src: '/media/android/android-chrome-72x72.png',
				sizes: '72x72',
				type: 'image/png',
				purpose: 'any maskable',
			},
			{
				src: '/media/android/android-chrome-96x96.png',
				sizes: '96x96',
				type: 'image/png',
				purpose: 'any maskable',
			},
			{
				src: '/media/android/android-chrome-144x144.png',
				sizes: '144x144',
				type: 'image/png',
				purpose: 'any maskable',
			},
			{
				src: '/media/android/android-chrome-192x192.png',
				sizes: '192x192',
				type: 'image/png',
				purpose: 'any maskable',
			},
			{
				src: '/media/android/android-chrome-256x256.png',
				sizes: '256x256',
				type: 'image/png',
				purpose: 'any maskable',
			},
			{
				src: '/media/android/android-chrome-384x384.png',
				sizes: '384x384',
				type: 'image/png',
				purpose: 'any maskable',
			},
			{
				src: '/media/android/android-chrome-512x512.png',
				sizes: '512x512',
				type: 'image/png',
				purpose: 'any maskable',
			},
		],
	},
	injectManifest: {
		globPatterns: ['**/*.{js,css,ico,png,svg,webp,woff,woff2}'],
	},
	workbox: {
		globPatterns: ['**/*.{js,css,ico,png,svg,webp,woff,woff2}'],
		sourcemap: true,
		navigateFallback: '/404',
		runtimeCaching: [
			{
				handler: 'NetworkOnly',
				urlPattern: /\/api\/.+json/,
				method: 'POST',
				options: {
					backgroundSync: {
						name: 'myQueueName',
						options: {
							maxRetentionTime: 24 * 60,
						},
					},
				},
			},
		],
	},
	devOptions: {
		enabled: true,
		type: 'module',
		navigateFallback: '/',
	},
	pwaAssets: {
		disabled: false,
		config: true,
		htmlPreset: '2023',
		overrideManifestIcons: true,
	},
};

export default pwaConfiguration;
